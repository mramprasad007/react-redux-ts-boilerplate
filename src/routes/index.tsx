import React from "react";
import { Route, Switch } from "react-router";
import Home from "../pages/home";
import Login from "../pages/login";

const routes = (
  <div>
    <Switch>
      <Route exact={true} path="/" component={Login} />
      <Route exact={true} path="/home" component={Home} />
    </Switch>
  </div>
);

export default routes;
