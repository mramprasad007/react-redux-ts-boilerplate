
import { Container, Typography } from "@material-ui/core";
import React from "react";
import { connect } from "react-redux";
import { RouteComponentProps } from "react-router";
import { RootStateType } from "../../store";
import styles from "./style";

const Home: React.FunctionComponent<RouteComponentProps<any> & HomeStatePropType> = (props) => {
  const classes = styles();
  return (
    <Container maxWidth="sm" >
      <div className={classes.header}>
        <Typography component="h2">
          Welcome Home
        </Typography>
      </div>
      <div>
        Token: {props.login.token}
      </div>
    </Container>
  );
};

const mapStateToProps = (state: RootStateType) => ({
  login: state.login,
});

type HomeStatePropType = ReturnType<typeof mapStateToProps>;

export default connect<HomeStatePropType, {}, RouteComponentProps<any>, RootStateType>
  (mapStateToProps)(Home);
