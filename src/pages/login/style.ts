import { makeStyles } from "@material-ui/styles";

const styles = makeStyles({
  header: {
    fontWeight: "bold",
    margin: "20px",
    textAlign: "center",
  },
  textField: {
    marginBottom: "40px",
  },
});

export default styles;
