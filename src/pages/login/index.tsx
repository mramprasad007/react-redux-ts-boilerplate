
import { Button, Container, TextField, Typography } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { RouteComponentProps } from "react-router";
import { bindActionCreators } from "redux";
import { RootStateType } from "../../store";
import { loginAction } from "../../store/login/actions";
import { ILoginForm } from "../../store/login/types";
import styles from "./style";

const Login: React.FunctionComponent<RouteComponentProps<any> & LoginStatePropType & LoginDispatchPropsType> =
  (props) => {
    const [form, setValues] = useState({
      email: "",
      password: "",
    });

    const classes = styles();

    useEffect(() => {
      setValues(props.login.loginForm);
    }, [props.login.loginForm]);

    const updateField = (e: any) => {
      setValues({
        ...form,
        [e.target.name]: e.target.value,
      });
    };

    const handlelogin = () => {
      props.loginAction(form);
    };

    return (
      <Container maxWidth="sm" >
        <div className={classes.header}>
          <Typography component="h1">
            {props.login.title}
          </Typography>
        </div>
        <div>
          <TextField
            value={form.email}
            name="email"
            onChange={updateField}
            label="Username"
            fullWidth={true}
            className={classes.textField}
          />
          <TextField
            value={form.password}
            name="password"
            type="password"
            onChange={updateField}
            label="Password"
            fullWidth={true}
            className={classes.textField}
          />
          <Button onClick={handlelogin} fullWidth={true}>Submit</Button>
        </div>
      </Container>
    );
  };

const mapStateToProps = (state: RootStateType) => ({
  login: state.login,
});

const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      loginAction: (form: ILoginForm) => loginAction(form),
    },
    dispatch,
  );

type LoginStatePropType = ReturnType<typeof mapStateToProps>;

type LoginDispatchPropsType = ReturnType<typeof mapDispatchToProps>;

export default connect<LoginStatePropType, LoginDispatchPropsType, RouteComponentProps<any>, RootStateType>
  (mapStateToProps, mapDispatchToProps)(Login);
