import { Epic, ofType } from "redux-observable";
import { ajax } from "rxjs/ajax";
import { map, mergeMap, switchMap } from "rxjs/operators";
import { rootActions, RootActionType, RootStateType } from "../";

const doLogin: Epic<RootActionType, RootActionType, RootStateType> = (action$, store) =>
  action$.pipe(
    ofType(rootActions.login.LOGIN_INPROGRESS),
    map((action: any) => action.payload),
    switchMap((payload) =>
      ajax
        .post("https://reqres.in/api/login", payload)
        .pipe(
          mergeMap((data) => [rootActions.login.loginSuccessAction(data.response),
          rootActions.router.push({ pathname: "/home" })]),
        ),
    ),
  );

export default doLogin;
