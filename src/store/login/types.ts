export interface ILoginState {
  title: string;
  loginForm: ILoginForm;
  token: string;
}

export interface ILoginForm {
  email: string;
  password: string;
}

export interface ILoginSuccessResponse {
  token: string;
}
