import { Record } from "immutable";
import { ActionType } from "typesafe-actions";
import * as loginActions from "./actions";
import { ILoginState } from "./types";

type LoginActions = ActionType<typeof loginActions>;

const LoginState = Record<ILoginState>({
  loginForm: {
    email: "eve.holt@reqres.in",
    password: "cityslicka",
  },
  title: "Login",
  token: "",
});

const initialLoginState = new LoginState();

const LoginReducer = (state = initialLoginState, action: LoginActions) => {
  switch (action.type) {
    case loginActions.LOGIN_SUCCESS:
      return state.setIn(["token"], action.payload.token);
    default:
      return state;
  }
};

export default LoginReducer;
