import { createAction } from "typesafe-actions";
import { ILoginForm, ILoginSuccessResponse } from "./types";

export const LOGIN_INPROGRESS = "LOGIN_INPROGRESS";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_FAILURE = "LOGIN_FAILURE";

export const loginAction = createAction(LOGIN_INPROGRESS, (resolve) =>
  (payload: ILoginForm) => resolve(payload));
export const loginSuccessAction = createAction(LOGIN_SUCCESS, (resolve) =>
  (payload: ILoginSuccessResponse) => resolve(payload));
export const loginFailureAction = createAction(LOGIN_FAILURE, (resolve) => (error: Error) => resolve(error));
