
import { connectRouter, routerActions } from "connected-react-router";
import { History } from "history";
import { combineReducers } from "redux";
import { combineEpics } from "redux-observable";
import { ActionType, StateType } from "typesafe-actions";
import loaderReducer from "./loader/reducers";
import * as loginActions from "./login/actions";
import doLogin from "./login/epics";
import loginReducer from "./login/reducers";

const rootReducer = (history: History) => combineReducers({
  loader: loaderReducer,
  login: loginReducer,
  router: connectRouter(history),
});

const rootActions = {
  login: loginActions,
  router: routerActions,
};

const rootEpic = combineEpics(doLogin);

export type RootActionType = ActionType<typeof rootActions>;
export type RootStateType = StateType<ReturnType<typeof rootReducer>>;
export { rootReducer, rootEpic, rootActions };
