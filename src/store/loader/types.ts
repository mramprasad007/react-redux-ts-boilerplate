export interface ILoaderState {
  loading: boolean;
  parallelLoaderCount: number;
}
