import { Record } from "immutable";
import { ActionType } from "typesafe-actions";
import * as loaderActions from "./actions";
import { ILoaderState } from "./types";

type LoaderActionsType = ActionType<typeof loaderActions>;

const LoginState = Record<ILoaderState>({
  loading: false,
  parallelLoaderCount: 0,
});

const initialLoginState = new LoginState();

const loaderReducer = (state = initialLoginState, action: LoaderActionsType) => {
  switch (action.type) {
    case loaderActions.LOADING_INPROGRESS:
      state.setIn(["parallelLoaderCount"], state.parallelLoaderCount + 1);
      state.setIn(["loading"], true);
      return state;
    case loaderActions.LOADING_COMPLETED:
      const parallelLoaderCount = (state.parallelLoaderCount - 1) > 0
        ? state.parallelLoaderCount - 1 : state.parallelLoaderCount;
      state.setIn(["parallelLoaderCount"], parallelLoaderCount);
      state.setIn(["loading"], parallelLoaderCount !== 0);
      return state;
    default:
      return state;
  }
};

export default loaderReducer;
