import { createAction } from "typesafe-actions";

export const LOADING_INPROGRESS = "LOADING_INPROGRESS";
export const LOADING_COMPLETED = "LOADING_COMPLETED";

export const loadingInprogessAction = createAction(LOADING_INPROGRESS);
export const loginSuccessAction = createAction(LOADING_COMPLETED);
