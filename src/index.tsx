
import { CssBaseline } from "@material-ui/core";
import { ThemeProvider } from "@material-ui/styles";
import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import App from "./App";
import configureStore, { history } from "./configureStore";
import theme from "./theme";

const store = configureStore();
ReactDOM.render(
  <Provider store={store}>
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <App history={history} />
    </ThemeProvider>
  </Provider>,
  document.getElementById("root"),
);
