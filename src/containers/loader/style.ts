import { makeStyles } from "@material-ui/styles";

const styles = makeStyles({
  backdrop: {
    zIndex: 9999,
  },
  progress: {
    left: "50vw",
    position: "absolute",
    top: "50vh",
    zIndex: 1000,
  },
});

export default styles;
