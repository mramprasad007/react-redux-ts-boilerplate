import { Backdrop, CircularProgress } from "@material-ui/core";
import React from "react";
import { connect } from "react-redux";
import { RouteComponentProps } from "react-router";
import { RootStateType } from "../../store";
import styles from "./style";

const Loader: React.FunctionComponent<RouteComponentProps<any> & LoaderStatePropType> = (props) => {
  const classes = styles();
  return (
    props.loader.loading ?
      (<div>
        <Backdrop open={true} className={classes.backdrop} />
        <CircularProgress className={classes.progress} />
      </div>)
      : null
  );
};

type LoaderStatePropType = ReturnType<typeof mapStateToProps>;

const mapStateToProps = (state: RootStateType) => ({
  loader: state.loader,
});

export default connect<LoaderStatePropType, {}, RouteComponentProps<any>, RootStateType>
  (mapStateToProps)(Loader);
